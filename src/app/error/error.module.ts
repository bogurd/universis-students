import { ErrorHandler, ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import {
    ErrorBaseComponent,
    HttpErrorComponent
} from "./components/error-base/error-base.component";
import { ErrorRoutingModule } from "./error-routing.routing";
import { ErrorService } from "./error.service";
import { environment } from '../../environments/environment';
import { ErrorsHandler } from "./error.handler";

declare var System;

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ErrorRoutingModule
    ],
    providers: [

    ],
    declarations: [
        ErrorBaseComponent,
        HttpErrorComponent
    ],
    exports: [
        ErrorBaseComponent,
        HttpErrorComponent
    ]
})
export class ErrorModule {

    constructor( @Optional() @SkipSelf() parentModule: ErrorModule,
                 private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/error.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ErrorModule,
            providers: [
                ErrorService,
                {
                    provide: ErrorHandler,
                    useClass: ErrorsHandler,
                }
            ]
        };
    }


}
