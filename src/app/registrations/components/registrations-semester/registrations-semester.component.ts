import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { ProfileService } from '../../../profile/services/profile.service';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { LocalizedDatePipe } from 'src/app/shared/localized-date.pipe';

@Component({
  selector: 'registrations-semester',
  templateUrl: 'registrations-semester.component.html'
})
export class RegistrationSemesterComponent implements OnInit {

  messages: string[] = [
    'Error',
    'Successful',
    'Available',
    'Postgraduate',
    'Deadline',
    'NotAvailableTill',
    'NotActiveStudent'
  ];

  params: any;
  message: any;
  action: any;
  disableBut: boolean;

  constructor(private _currentRegistrationService: CurrentRegistrationService,
              private _translateService: TranslateService,
              private _profileService: ProfileService,
              private _router: Router,
              private _loading: LoadingService) { }

  ngOnInit() {
    this._loading.showLoading();
    this._currentRegistrationService.getRegistrationStatus().then(value => {
        this._profileService.getStudent().then(student => {
            const formatter = new LocalizedDatePipe(this._translateService);
            // initialize message params
            this.params = Object.assign({
                'registrationPeriodStartDate': formatter.transform(student.department.registrationPeriodStart, 'mediumDate'),
                'registrationPeriodEndDate': formatter.transform(student.department.registrationPeriodEnd, 'mediumDate')
            }, student.department);

            // Can be redirected to CoursesRegistration?
            this._currentRegistrationService.getCurrentRegistrationEffectiveStatus().then( res => {
              let courses;
              if ( res.code === 'OPEN_NO_TRANSACTION'
                || res.code ===  'PENDING_TRANSACTION'
                || res.code ===  'SUCCEEDED_TRANSACTION'
                || res.code ===  'FAILED_TRANSACTION'
                || res.code === 'P_SUCCEEDED_TRANSACTION'
                || res.code ===  'P_SYSTEM_TRANSACTION') {
                  courses = true;
              } else { courses = false; }
              // Get Action for button
              this._translateService.get(this.message.action).subscribe(act => {
                this.action = act;
                if (act === '2') { this.disableBut = !courses; }
                this._loading.hideLoading();
              });
            });
            const statusText = this.messages[value];
            this.message = {
                'icon': `SemesterRegistration.${statusText}.icon`,
                'info': `SemesterRegistration.${statusText}.info`,
                'message': `SemesterRegistration.${statusText}.message`,
                'extraMessage': `SemesterRegistration.${statusText}.extraMessage`,
                'actionButton': `SemesterRegistration.${statusText}.actionButton`,
                'actionText': `SemesterRegistration.${statusText}.actionText`,
                'action': `SemesterRegistration.${statusText}.action`
            };
        });
    });
  }

  open() {
    switch (this.action) {
      case '1':
        this.registerSemester();
        break;
      case '2':
        this.registerCourses();
        break;
    }
  }

  registerSemester() {
    this._currentRegistrationService.registerSemester();
  }

  registerCourses() {
        this._router.navigateByUrl('/registrations/courses');
  }
}
