import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradesProjectComponent } from './grades-project.component';

describe('GradesProjectComponent', () => {
  let component: GradesProjectComponent;
  let fixture: ComponentFixture<GradesProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradesProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
